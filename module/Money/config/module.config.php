<?php

namespace Money;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'category' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/category[/:action[/:id]]',
                    'defaults' => [
                        'controller' => Controller\CategoryController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'company' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/company[/:action[/:id]]',
                    'defaults' => [
                        'controller' => Controller\CompanyController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'transaction' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/transaction[/:action]',
                    'defaults' => [
                        'controller' => Controller\TransactionController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'import' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/import[/:action]',
                    'defaults' => [
                        'controller' => Controller\ImportController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'report' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/report[/:action]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'rules' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/rules[/:action[/:id]]',
                    'defaults' => [
                        'controller' => Controller\RulesController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'import_console' => [
                    'options' => [
                        'route' => 'import',
                        'defaults' => [
                            'controller' => Controller\ImportController::class,
                            'action' => 'import',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\CategoryController::class => Controller\Factory\DoctrineControllerFactory::class,
            Controller\CompanyController::class => Controller\Factory\DoctrineControllerFactory::class,
            Controller\TransactionController::class => Controller\Factory\DoctrineControllerFactory::class,
            Controller\ImportController::class => Controller\Factory\ImportControllerFactory::class,
            Controller\ReportController::class => Controller\Factory\ReportControllerFactory::class,
            Controller\RulesController::class => Controller\Factory\DoctrineControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',

        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'money_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Money\Entity' => 'money_entity',
                ]
            ],

        ],
    ],
    'service_manager' => [
        'factories' => array(
            Service\ImportService::class => Service\Factory\DoctrineServiceFactory::class,
            Service\ReportService::class => Service\Factory\DoctrineServiceFactory::class
        ),
    ],

];
