<?php

namespace Money\Service;

use Doctrine\ORM\EntityManager;
use Money\Entity\Repository\TransactionRepository;
use Money\Entity\Transaction;

class ReportService
{
    /** @var  EntityManager $entityManager */
    private $entityManager;

    /**
     * ReportService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return bool|array
     */
    public function report()
    {
        /** @var  $transaction TransactionRepository */
        $transaction = $this->entityManager->getRepository(Transaction::class);
        $report = $transaction->getReport();
        if (!$report) {
            return false;
        }

        $reportByMonths = [];
        foreach ($report as $item) {
            $reportByMonths[$item['month']][] = [
                'name' => $item['name'],
                'total_sum' => $item['total_sum']
            ];
        }
        ksort($reportByMonths);
        $reportByMonths = $this->replaceMonth($reportByMonths);

        return [
            'reportByMonths' => $reportByMonths,
            'total' => $this->countTotal($reportByMonths)
        ];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function countTotal($data)
    {
        $result = [];
        foreach ($data as $month => $item) {
            $result[$month] = 0;
            foreach ($item as $value) {
                $result[$month] += $value['total_sum'];
            }
        }
        return $result;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function replaceMonth($data)
    {
        $months = array(
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь'
        );
        $result = [];
        foreach ($data as $m => $v) {
            $result[$months[$m]] = $v;
        }
        return $result;
    }
}