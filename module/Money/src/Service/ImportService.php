<?php

namespace Money\Service;

use Doctrine\ORM\EntityManager;
use Money\Entity\Category;
use Money\Entity\RulesCompanyCategory;
use Money\Entity\Transaction;
use Money\Entity\Company;
use Smalot\PdfParser\Parser;

class ImportService
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var string $pattern
     */
    private $pattern = '/([0-9]{2}\.[0-9]{2}\.[0-9]{6}\.[0-9]{2}\.[0-9]{4})Оплата товаров и услуг с помощью карты в (.*), , RUS(|\s)(\d+?\s\d+?|\d+?),(\d+?)/Ui';

    /**
     * @var string $fileName
     */
    private $fileName = 'card_min.pdf';

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function import()
    {
        $text = $this->load();
        $data = $this->parse($text);
        $this->process($data);
    }

    public function delete()
    {
        $this->truncate(['company', 'transaction']);
    }

    /**
     * @param array $tableNames
     * @param bool $cascade
     */
    private function truncate($tableNames = array(), $cascade = false)
    {
        $connection = $this->entityManager->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tableNames as $name) {
            $connection->executeUpdate($platform->getTruncateTableSQL($name, $cascade));
        }
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }

    /**
     * @return string
     */
    private function load()
    {
        $parser = new Parser();
        $pdf = $parser->parseFile($this->fileName);
        $text = $pdf->getText();
        return $text;
    }

    /**
     * @param string $text
     * @return array
     */
    private function parse($text)
    {
        $text = str_replace("\n", " ", $text);
        preg_match_all($this->pattern, $text, $out);
        return $out;
    }

    /**
     * @param array $data
     */
    private function process($data)
    {
        $date = $data[1];
        $companies = $data[2];
        $sumRub = $data[4];
        $sumCop = $data[5];

        $rules = $this->entityManager->getRepository(RulesCompanyCategory::class)->findAll();

        $i = 0;
        foreach ($companies as $companyName) {
            $category = 'Все';
            foreach ($rules as $rule) {
                if (stristr($companyName, $rule->getCompany())) {
                    $category = $rule->getCategory()->getName();
                }
            }
            $category = $this->entityManager->getRepository(Category::class)->findOneBy(['name' => $category]);
            $company = new Company();
            $company->setName($companyName);
            $company->setCategory($category);
            $this->entityManager->persist($company);

            $sourceDate = substr($date[$i], 0, 10);
            $sourceSum = str_replace(' ', '', $sumRub[$i]) . '.' . $sumCop[$i];
            $transaction = new Transaction();
            $transaction->setCompany($company);
            $transaction->setDate(new \DateTime($sourceDate));
            $transaction->setSum($sourceSum);
            $this->entityManager->persist($transaction);

            $i++;
        }
        $this->entityManager->flush();
    }
}