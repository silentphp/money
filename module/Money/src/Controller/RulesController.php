<?php

namespace Money\Controller;

use Doctrine\ORM\EntityManager;
use Money\Entity\RulesCompanyCategory;
use Money\Form\RulesForm;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class RulesController extends AbstractActionController
{
    /** @var EntityManager $entityManger */
    private $entityManger;

    /**
     * RulesController constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManger = $entityManager;
    }

    public function indexAction()
    {
        $rules = $this->entityManger->getRepository(RulesCompanyCategory::class)->findAll();
        return new ViewModel(['rules' => $rules]);
    }

    public function addAction()
    {
        return $this->editAction();
    }

    public function editAction()
    {
        $category = new RulesCompanyCategory();
        if ($this->params('id') > 0) {
            $category = $this
                ->entityManger
                ->getRepository(RulesCompanyCategory::class)
                ->find($this->params('id'));
        }
        $form = new RulesForm($this->entityManger);
        $form->bind($category);
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->entityManger->persist($category);
                $this->entityManger->flush();
                return $this->redirect()->toRoute('rules');
            }
        }
        return new ViewModel(['form' => $form]);
    }
}