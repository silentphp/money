<?php

namespace Money\Controller;

use Money\Form\CategoryForm;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Money\Entity\Category;

class CategoryController extends AbstractActionController
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * CategoryController constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function indexAction()
    {
        $category = $this->entityManager->getRepository(Category::class)->findAll();
        return new ViewModel(['category' => $category]);
    }

    public function addAction()
    {
        return $this->editAction();
    }

    public function editAction()
    {
        $category = new Category();

        if ($this->params('id') > 0) {
            $category = $this
                ->entityManager
                ->getRepository(Category::class)
                ->find($this->params('id'));
        }

        $form = new CategoryForm($this->entityManager);
        $form->bind($category);
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->entityManager->persist($category);
                $this->entityManager->flush();
                return $this->redirect()->toRoute('category');
            }
        }
        return new ViewModel(['form' => $form]);
    }

    public function deleteAction()
    {
        $category = $this
            ->entityManager
            ->getRepository(Category::class)
            ->find($this->params('id'));
        if ($category) {
            $this->entityManager->remove($category);
            $this->entityManager->flush();
        }
        return $this->redirect()->toRoute('category');
    }
}