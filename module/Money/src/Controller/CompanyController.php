<?php

namespace Money\Controller;

use Doctrine\ORM\EntityManager;
use Money\Entity\Company;
use Money\Form\CompanyForm;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CompanyController extends AbstractActionController
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * CompanyController constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function indexAction()
    {
        $company = $this->entityManager->getRepository(Company::class)->findAll();
        return new ViewModel(['company' => $company]);
    }

    public function addAction()
    {
        return $this->editAction();
    }

    public function editAction()
    {
        $company = new Company();
        if ($this->params('id') > 0) {
            $company = $this->entityManager
                ->getRepository(Company::class)
                ->find($this->params('id'));
        }
        $form = new CompanyForm($this->entityManager);
        $form->bind($company);
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->entityManager->persist($company);
                $this->entityManager->flush();
                return $this->redirect()->toRoute('company');
            }
        }

        return new ViewModel(['form' => $form]);
    }
}