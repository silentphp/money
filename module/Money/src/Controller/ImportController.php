<?php

namespace Money\Controller;

use Doctrine\ORM\EntityManager;
use Money\Service\ImportService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ImportController extends AbstractActionController
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /** @var ImportService $importService */
    private $importService;

    /**
     * ImportController constructor.
     * @param EntityManager $entityManager
     * @param ImportService $importService
     */
    public function __construct(EntityManager $entityManager, ImportService $importService)
    {
        $this->entityManager = $entityManager;
        $this->importService = $importService;
    }

    public function deleteAction()
    {
        $this->importService->delete();
        return new ViewModel();
    }

    public function importAction()
    {
        $this->importService->import();
    }

}