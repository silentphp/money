<?php

namespace Money\Controller;

use Doctrine\ORM\EntityManager;
use Money\Service\ReportService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /** @var  ReportService $reportService */
    private $reportService;

    /**
     * ReportController constructor.
     * @param EntityManager $entityManager
     * @param ReportService $reportService
     */
    public function __construct(EntityManager $entityManager, ReportService $reportService)
    {
        $this->entityManager = $entityManager;
        $this->reportService = $reportService;
    }

    public function indexAction()
    {
        $result = $this->reportService->report();
        return new ViewModel(['reportByMonths' => $result['reportByMonths'], 'total' => $result['total']]);
    }
}