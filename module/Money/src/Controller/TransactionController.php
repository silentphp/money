<?php

namespace Money\Controller;

use Doctrine\ORM\EntityManager;
use Money\Entity\Transaction;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TransactionController extends AbstractActionController
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * TransactionController constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function indexAction()
    {
        $transactions = $this->entityManager->getRepository(Transaction::class)->findBy([], ['date' => 'ASC']);
        return new ViewModel(['transactions' => $transactions]);
    }

}