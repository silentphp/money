<?php

namespace Money\Form;

use Doctrine\ORM\EntityManager;
use Zend\Form\Form;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Money\Entity\Company;

class CompanyForm extends Form
{
    /**
     * CompanyForm constructor.
     * @param EntityManager $entityManger
     */
    public function __construct($entityManger)
    {
        parent::__construct('category-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManger, Company::class));
        $this->add([
            'type' => 'hidden',
            'name' => 'id'
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'title'
            ],
            'options' => [
                'label' => 'Title',
            ],
        ]);
        $this->add([
            'name' => 'category',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => [
                'label' => 'Category',
                'object_manager' => $entityManger,
                'target_class' => 'Money\Entity\Category',
                'property' => 'name'
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }
}