<?php

namespace Money\Form;

use Doctrine\ORM\EntityManager;
use Zend\Form\Form;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Money\Entity\Category;

class CategoryForm extends Form
{
    /**
     * CategoryForm constructor.
     * @param EntityManager $entityManager
     */
    public function __construct($entityManager)
    {
        parent::__construct('category-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, Category::class));
        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'title'
            ],
            'options' => [
                'label' => 'Title',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }
}