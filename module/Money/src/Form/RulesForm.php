<?php

namespace Money\Form;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Money\Entity\RulesCompanyCategory;
use Zend\Form\Form;

class RulesForm extends Form
{
    /**
     * RulesForm constructor.
     * @param EntityManager $entityManger
     */
    public function __construct($entityManger)
    {
        parent::__construct('rules-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManger, RulesCompanyCategory::class));
        $this->add([
            'type' => 'hidden',
            'name' => 'id'
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'company',
            'attributes' => [
                'id' => 'company'
            ],
            'options' => [
                'label' => 'company',
            ],
        ]);
        $this->add([
            'name' => 'category',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => [
                'label' => 'Category',
                'object_manager' => $entityManger,
                'target_class' => 'Money\Entity\Category',
                'property' => 'name'
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }
}