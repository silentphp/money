<?php

namespace Money\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getReport()
    {
        $entityManager = $this->getEntityManager();
        $conn = $entityManager->getConnection();
        $sql = 'select MONTH(t.date) as month, ct.name, SUM(t.sum) as total_sum from transaction t
            left join company  c on c.id = t.company_id
            left join category ct on ct.id = c.category_id
            group by ct.name, month(date)
            order by total_sum DESC';
        $query = $conn->query($sql);
        $result = $query->fetchAll();

        return $result ?: [];
    }
}