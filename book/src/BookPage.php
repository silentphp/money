<?php

class BookPage
{
    /**
     * @var BooksData $dataSource
     */
    private $dataSource;

    private $fileNamePageAllBook = 'all_list.json';

    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function getBookPage(){
        $data = $this->getAllBookPage();
        $jsonData = json_decode($data);
        $i = 0;
        foreach ($jsonData as $item) {
            foreach ($item as $itemBook) {
                $bookData = $this->getCachePageBook($itemBook->id);
                if(!$bookData){
                    $this->dataSource->setUrl("http://eldorado.miflib.ru/books/" . $itemBook->id . ".ajax");
                    $bookData = $this->dataSource->getData();
                    $this->savePageBook($itemBook->id, $bookData);
                }

                $i++;
                if($i > 2){
                    break 2;
                }

            }

        }
        return true;
    }

    private function getAllBookPage()
    {
        $data = $this->getCachePageAllBook();
        if(!$data){
            $data = $this->dataSource->getData();
            $this->savePageAllBook($data);
            return $data;
        }
        return $data;
    }

    private function getCachePageAllBook(){
        if(file_exists('cache/' . $this->fileNamePageAllBook)){
            return file_get_contents('cache/' . $this->fileNamePageAllBook);
        }
        return false;
    }

    private function getCachePageBook($fileName){
        if(file_exists('cache/book/' . $fileName)){
            return file_get_contents('cache/book/' . $fileName);
        }
        return false;
    }

    private function savePageAllBook($data){
        return $this->save('cache/'.$this->fileNamePageAllBook, $data);
    }

    private function savePageBook($fileName, $data){
        return $this->save('cache/book/'.$fileName, $data);
    }


    /**
     * @param string $fileName
     * @param string $data
     * @return int
     */
    private function save($fileName, $data){
       return file_put_contents($fileName, $data);
    }

}