<?php

class BooksData implements DataSource

{
    private $url = '';

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        if(!$this->url){
            return false;
        }
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=5p25k4bu6r6dd8kotheae1dcc7");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $output = curl_exec($ch);
        return $output;
    }

}